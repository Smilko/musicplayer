package at.htl.songfinder.finder;

import at.htl.songfinder.config.Properties;
import at.htl.songfinder.config.Property;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class FileFinder {
    private final String STARTPATH = Properties.getProperty(Property.startPath);
    private final String FILEENDING = ".mp3";
    private List<Path> mp3Files;

    public FileFinder() throws NoSuchFieldException, IOException {
    }

    public List<Path> startFinding() throws IOException {
        System.out.println("Starting to look for files ...");
        mp3Files = new LinkedList<Path>();
        lookForFiles(Files.newDirectoryStream(Paths.get(STARTPATH)));
        System.out.println("Search finished");
        System.out.println("Total number of files found: " + mp3Files.size());
        return mp3Files;
    }

    private void lookForFiles(DirectoryStream<Path> directory) throws IOException {
        //File[] files = directory.listFiles();

        for (Path file : directory) {
            if(Files.isDirectory(file)){
                System.out.println("Looking for files in sub-directory: " + file.toString());
                lookForFiles(Files.newDirectoryStream(file));
            }
            else if(file.getFileName().toString().endsWith(FILEENDING)){
                mp3Files.add(file);
                //System.out.println(file);
            }
        }
    }
}
