package at.htl.songfinder.utils;

import org.apache.commons.codec.binary.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.List;

public class FileToJsonParser {
    public static String FileListToJson(List<Path> files){
        JSONObject object = new JSONObject();
        JSONArray fileArray = new JSONArray();
        for (Path f : files) {
            fileArray.put(FileToJson(f));
        }
        object.put("files", fileArray);
        return object.toString();
    }

    public static JSONObject FileToJson(Path file){
        JSONObject jsonObject = new JSONObject();
//        System.out.println("Adding: " + file.getFileName());
        System.out.println(file.toString());
        jsonObject.put("filePath", new String(Base64.getEncoder().encode(file.toString().getBytes())));
        jsonObject.put("title", file.getFileName().toString());
        return jsonObject;
    }
}
