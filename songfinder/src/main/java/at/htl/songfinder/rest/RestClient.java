package at.htl.songfinder.rest;

import at.htl.songfinder.entity.User;
import at.htl.songfinder.utils.FileToJsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpRetryException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Base64;
import java.util.List;

import static org.apache.http.entity.mime.MIME.UTF8_CHARSET;

public class RestClient
{
    private String token = null;
    private BasicRestCalls basicCalls;
    private static RestClient instance = null;

    private RestClient(){
        basicCalls = BasicRestCalls.getInstance();
    }

    public static RestClient getInstance(){
        if(instance == null){
            instance = new RestClient();
        }
        return instance;
    }

    public void sendFiles(List<Path> fileList) throws IOException, NoSuchFieldException {
        System.out.println("Starting parsing ...");
        String json = FileToJsonParser.FileListToJson(fileList);
        System.out.println("Parsing finished");
        System.out.println("Sending json");
        BasicRestCalls.getInstance().post("song/addSongs", new StringEntity(json.toString()), token);
    }

    public boolean login(User u){
        try {
            String s = u.parseJson().toString();
            HttpResponse response = basicCalls.post("access/login", new StringEntity(s), token);
            //StringEntity entity = (StringEntity) response.getEntity();
            token = extractString(response.getEntity());
            System.out.println("Got token: " + token);
            return true;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    private String extractString(HttpEntity entity) throws IOException {
        return EntityUtils.toString(entity, UTF8_CHARSET).toString();
    }

    public void logout(User admin) {

    }
}
