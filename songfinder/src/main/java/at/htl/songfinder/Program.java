package at.htl.songfinder;

import at.htl.songfinder.entity.User;
import at.htl.songfinder.finder.FileFinder;
import at.htl.songfinder.rest.RestClient;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class Program {
    public static void main(String[] args) throws NoSuchFieldException, IOException {
        User admin = new User("admin", "admin", "", "");
        FileFinder fileFinder = new FileFinder();
        List<Path> files = fileFinder.startFinding();

        RestClient client = RestClient.getInstance();
        client.login(admin);
        client.sendFiles(files);
        client.logout(admin);

        System.out.println("Program finished");
    }
}
