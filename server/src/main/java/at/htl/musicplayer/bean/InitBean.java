package at.htl.musicplayer.bean;

import at.htl.musicplayer.entity.Admin;
import at.htl.musicplayer.entity.User;
import at.htl.musicplayer.facade.AdminFacade;
import at.htl.musicplayer.facade.UserFacade;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.List;

@Singleton
@Startup
public class InitBean {
    @Inject
    AdminFacade adminFacade;

    @Inject
    UserFacade userFacade;


    @PostConstruct
    public void init(){
        if(adminFacade.getAdminCount() < 1){
            User admin = userFacade.getUserByUsername("admin");
            if (admin == null){
                admin = new User("admin", "admin", "Stefan", "Smiljkovic");
                userFacade.save(admin);
            }
            adminFacade.save(new Admin(admin));
        }
    }
}
