package at.htl.musicplayer.facade;

import at.htl.musicplayer.entity.Artist;
import at.htl.musicplayer.facade.contract.GenericFacade;

import javax.ejb.Singleton;

@Singleton
public class ArtistFacade extends GenericFacade<Artist> {
}
