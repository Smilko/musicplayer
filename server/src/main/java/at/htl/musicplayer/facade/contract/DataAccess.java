package at.htl.musicplayer.facade.contract;

import java.util.List;

public interface DataAccess<E> {
    void save(E e);
    E get(long id);
    void update(E e);
    void delete(E e);
}
