package at.htl.musicplayer.facade.contract;

import at.htl.musicplayer.entity.Artist;
import at.htl.musicplayer.entity.User;
import at.htl.musicplayer.entity.contract.EntityClass;
import at.htl.musicplayer.facade.contract.DataAccess;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.NotFoundException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class GenericFacade<E extends EntityClass> implements DataAccess<E> {
    @PersistenceContext
    protected EntityManager em;
    private Class<E> c;

    public GenericFacade(){
        Type superclass = getClass().getGenericSuperclass();
        ParameterizedType parameterized = null;
        while (parameterized == null) {
            if ((superclass instanceof ParameterizedType)) {
                parameterized = (ParameterizedType) superclass;
            } else {
                superclass = ((Class<E>) superclass).getGenericSuperclass();
            }
        }
        this.c = (Class<E>) parameterized.getActualTypeArguments()[0];
    }

    @Override
    public void save(E e) {
        em.persist(e);
    }

    @Override
    public E get(long id) {
        try {
            String tablename = c.getAnnotationsByType(Entity.class)[0].name();
            tablename = tablename.equals("") ? c.getName() : tablename;
            return (E) em.createQuery("select e from "+ tablename +" e where e.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
        }catch (NoResultException e){
            System.err.println("No entity found for query where id = " + id);
            return null;
        }
    }

    @Override
    public void update(E e) {
        if(get(e.getId()) != null){
            throw new NotFoundException(e.getClass() + " is not in Database!");
        }
        delete(e);
        save(e);
    }

    @Override
    public void delete(E e) {
        em.remove(e);
    }
}
