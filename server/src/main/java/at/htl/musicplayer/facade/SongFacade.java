package at.htl.musicplayer.facade;

import at.htl.musicplayer.entity.Song;
import at.htl.musicplayer.facade.contract.GenericFacade;

import javax.ejb.Singleton;
import java.util.List;

@Singleton
public class SongFacade extends GenericFacade<Song> {
    public void saveAll(List<Song> songs){
        for (Song s : songs) {
            save(s);
        }
    }

    public List<Song> getSongsContaining(String findby){
        String findString = "'%" + findby.toLowerCase() + "%'";
        return em.createQuery("select s from Song s where lower(s.name) like " + findString)
                .getResultList();
    }
}
