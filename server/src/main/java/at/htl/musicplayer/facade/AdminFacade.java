package at.htl.musicplayer.facade;

import at.htl.musicplayer.entity.Admin;
import at.htl.musicplayer.entity.User;
import at.htl.musicplayer.facade.contract.GenericFacade;

import javax.ejb.Singleton;
import java.util.List;

@Singleton
public class AdminFacade extends GenericFacade<Admin> {

    public List<User> getAllAdmins(){
        return em.createQuery("select a from Admin a").getResultList();
    }

    public long getAdminCount(){
        return (long) em.createQuery("select count(a) from appUser a").getSingleResult();
    }

    public boolean isUserAnAdmin(long uid){
        long count = (long) em.createQuery("select count(a) from Admin a where a.user.id = :uid")
                .setParameter("uid", uid).getSingleResult();
        return count != 0;
    }
}
