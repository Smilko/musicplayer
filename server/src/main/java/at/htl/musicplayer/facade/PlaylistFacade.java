package at.htl.musicplayer.facade;

import at.htl.musicplayer.entity.Playlist;
import at.htl.musicplayer.facade.contract.GenericFacade;

import javax.ejb.Singleton;

@Singleton
public class PlaylistFacade extends GenericFacade<Playlist> {
}
