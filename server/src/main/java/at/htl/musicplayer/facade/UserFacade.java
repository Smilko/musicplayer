package at.htl.musicplayer.facade;


import at.htl.musicplayer.entity.Artist;
import at.htl.musicplayer.entity.User;
import at.htl.musicplayer.facade.contract.GenericFacade;

import javax.ejb.Singleton;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.NotFoundException;
import java.util.List;

@Singleton
public class UserFacade extends GenericFacade<User> {

    public List<User> getUsersContainingString(String search){
        String lookFor = '*' + search + '*';
        return em.createQuery("select u from appUser u where u.username like :s or u.firstname like :s or u.lastname like :s")
                .setParameter("s", lookFor)
                .getResultList();
    }

    public long getUserCount(){
        return (long) em.createQuery("select count(1) from appUser").getSingleResult();
    }

    public User getUserByUsername(String username){
        try{
            return (User) em.createQuery("select u from appUser u where u.username = :username")
                    .setParameter("username", username)
                    .getSingleResult();
        }catch (NoResultException e){
            System.err.println("No entity found for query in getUserByUsername where username = " + username);
            return null;
        }
    }
}
