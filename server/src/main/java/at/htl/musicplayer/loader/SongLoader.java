package at.htl.musicplayer.loader;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Base64;

public class SongLoader {
    public static String getMusicFileAsBase64(String path) throws IOException, URISyntaxException {
        String decodedPath = new String(Base64.getDecoder().decode(path.getBytes()));
        System.out.println(decodedPath);
        FileInputStream is = new FileInputStream(decodedPath);
        byte[] ar = IOUtils.toByteArray(is);
        return new String(Base64.getEncoder().encode(ar));
    }
}
