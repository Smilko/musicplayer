package at.htl.musicplayer.entity;

import at.htl.musicplayer.entity.contract.EntityClass;
import org.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Song implements EntityClass, Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    private Artist artist;

    @Column(name = "s_name", length = 1000)
    private String name;

    @Column(name = "s_filepath", length = 1000)
    String path;

    //region Constr
    public Song(Artist artist, String name, String path) {
        this.artist = artist;
        this.name = name;
        this.path = path;
    }

    public Song() {
    }
    //endregion

    //region Getter and Setter

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artistid) {
        this.artist = artistid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    //endregion

    public JSONObject getJsonObject(){
        return getJsonObject(false);
    }

    public JSONObject getJsonObject(boolean putpath){
        JSONObject json = new JSONObject();
        json.put("id", getId());
        json.put("artistId", getArtist());
        json.put("name", getName());
        if (putpath)
            json.put("path", getPath());
        return json;
    }
}