package at.htl.musicplayer.entity;

import at.htl.musicplayer.entity.contract.EntityClass;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Artist implements EntityClass, Serializable{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "a_name")
    private String name;

    //region Constructor
    public Artist(String name) {
        this.name = name;
    }

    public Artist() {
    }
    //endregion

    //region Getter and Setter
    @Override
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    //endregion

}
