package at.htl.musicplayer.entity.contract;

import javax.annotation.Generated;
import javax.persistence.*;

public interface EntityClass {
    long getId();
}
