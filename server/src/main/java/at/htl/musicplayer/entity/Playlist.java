package at.htl.musicplayer.entity;

import at.htl.musicplayer.entity.contract.EntityClass;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Playlist implements EntityClass, Serializable{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @Column(name = "p_name")
    private long name;

    @ManyToMany(targetEntity = Song.class, fetch = FetchType.EAGER)
    private List<Song> songs;

    //region Constr
    public Playlist(User user, long name) {
        this.user = user;
        this.name = name;
    }

    public Playlist() {
    }
    //endregion

    //region Getter and Setter

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getName() {
        return name;
    }

    public void setName(long name) {
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }
    //endregion
}
