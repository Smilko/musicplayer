package at.htl.musicplayer.entity;

import at.htl.musicplayer.entity.contract.EntityClass;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Admin implements EntityClass, Serializable{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    @Override
    public long getId() {
        return id;
    }

    public Admin() {
    }

    public Admin(User userId) {
        this.user = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User userId) {
        this.user = userId;
    }
}
