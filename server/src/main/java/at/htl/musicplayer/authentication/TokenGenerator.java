package at.htl.musicplayer.authentication;

import com.sun.jmx.snmp.SnmpUnsignedInt;

import java.util.List;
import java.util.Random;

public class TokenGenerator {
    public static String GererateToken(List<Token> otherTokens){
        Random r = new Random();
        String newToken = null;

        do{
            StringBuilder s = new StringBuilder();
            s.append(generateRandomToken());
            s.append(generateRandomToken());
            s.append(generateRandomToken());
            newToken = s.toString();
        }while (otherTokens.contains(newToken));

        return newToken;
    }

    private static String generateRandomToken(){
        final int LENGTH = 20;
        StringBuilder token = new StringBuilder();
        Random r = new Random();

        for (int i = 0; i < LENGTH; i++) {
            token.append((char) r.nextInt());
        }

        long hash = Integer.toUnsignedLong(token.toString().hashCode());
        System.out.println(hash);
        return Long.toString(hash);
    }
}
