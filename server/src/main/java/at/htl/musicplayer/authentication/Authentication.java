package at.htl.musicplayer.authentication;

import at.htl.musicplayer.entity.User;
import at.htl.musicplayer.facade.AdminFacade;
import at.htl.musicplayer.facade.UserFacade;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

@Singleton
public class Authentication {
    List<Token> tokens;
    Semaphore tokenAccess = new Semaphore(1);

    @Inject
    UserFacade userFacade;

    @Inject
    AdminFacade adminFacade;

    public Authentication(){
        tokens = new LinkedList<>();
    }

    public String createToken(User u) throws InterruptedException {
        tokenAccess.acquire();
        Token listToken = findToken(u.getUsername());
        boolean passwordOk = isPasswordRight(u);
        if(listToken != null && passwordOk){
            tokenAccess.release();
            return listToken.getToken();
        }
        if (passwordOk == false){
            tokenAccess.release();
            return null;
        }

        String t = TokenGenerator.GererateToken(tokens);
        User loadedUser = userFacade.getUserByUsername(u.getUsername());
        System.out.println(loadedUser.getId());
        System.out.println(loadedUser.getUsername());
        System.out.println(loadedUser.getPassword());
        System.out.println(loadedUser.getFirstname());
        Token token = new Token(t, loadedUser, adminFacade.isUserAnAdmin(loadedUser.getId()));
        tokens.add(token);

        tokenAccess.release();
        return token.getToken();
    }

    public boolean removeToken(String t, User u) throws InterruptedException {
        tokenAccess.acquire();
        if (isTokenValid(t) && isPasswordRight(u)){
            tokens.remove(t);
            tokenAccess.release();
            return true;
        }
        tokenAccess.release();
        return false;
    }

    private Token findToken(String username){
        for (Token t: tokens) {
            if(t.getUser().getUsername().equals(username))
                return t;
        }
        return null;
    }

    private boolean isPasswordRight(User u){
        User internalUser = userFacade.getUserByUsername(u.getUsername());
        if(internalUser == null)
            return false;
        return internalUser.isValid(u);
    }

    public boolean isTokenFromAdmin(String token){
        return getToken(token).isAdmin();
    }

    private Token getToken(String tokenString){
        for (Token t : tokens) {
            if(t.getToken().equals(tokenString))
                return t;
        }
        return null;
    }

    public boolean isTokenValid(String token){
        return getToken(token) != null;
    }
}
