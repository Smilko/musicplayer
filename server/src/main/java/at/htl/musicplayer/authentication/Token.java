package at.htl.musicplayer.authentication;

import at.htl.musicplayer.entity.User;

public class Token {
    private String token;
    private User user;
    private boolean isAdmin;

    //region Constr
    public Token(String token, User user, boolean isAdmin) {
        this.token = token;
        this.user = user;
        this.isAdmin = isAdmin;
    }

    public Token() {
    }
    //endregion

    //region Getter and Setter
    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
    //endregion

    @Override
    public boolean equals(Object obj) {
        String otherToken = (String) obj;
        return otherToken.equals(this.token);
    }
}
