package at.htl.musicplayer.rest;

import at.htl.musicplayer.authentication.Authentication;
import at.htl.musicplayer.entity.User;
import at.htl.musicplayer.facade.UserFacade;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;

@Path("user")
public class UserAccess {
    @Inject
    Authentication authentication;

    @Inject
    UserFacade userFacade;

    @GET
    @Path("/searachfor={constr}")
    public Response getPersonWithString(@PathParam("constr")String searchConstraint, @HeaderParam("token") String token){
        if (!authentication.isTokenValid(token)){
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        List<User> users = userFacade.getUsersContainingString(searchConstraint);
        List<JSONObject> userObjects = new LinkedList<>();
        for (User u : users) {
               userObjects.add(u.parseJson(false));
        }
        JSONArray userArray = new JSONArray();
        for (JSONObject userJson : userObjects) {
            userArray.put(userJson);
        }
        JSONObject response = new JSONObject();
        response.put("searchconstraint", searchConstraint);
        response.put("users", userArray);

        return Response.ok().entity(response.toString()).build();
    }
}
