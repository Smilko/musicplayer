package at.htl.musicplayer.rest;

import at.htl.musicplayer.authentication.Authentication;
import at.htl.musicplayer.entity.User;
import at.htl.musicplayer.facade.UserFacade;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("register")
public class RegisterAccess {
    @Inject
    UserFacade userFacade;

    @Inject
    Authentication authentication;

    @POST
    public Response register(String jsonBody){
        User u = User.createUserFromJson(jsonBody);
        User internal = userFacade.getUserByUsername(u.getUsername());
        if(internal != null){
            return Response.status(Response.Status.CONFLICT).entity("User already exists").build();
    }
        userFacade.save(u);
        return Response.status(Response.Status.OK).build();
    }
}
