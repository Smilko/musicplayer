package at.htl.musicplayer.rest;

import at.htl.musicplayer.authentication.Authentication;
import at.htl.musicplayer.entity.User;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.ok;

@Path("access")
public class AuthenticationAccess {

    @Inject
    Authentication authentication;

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response logIn(String jsonBody) throws InterruptedException {
        System.out.println(jsonBody);
        User u = User.createUserFromJson(jsonBody);
        String userToken = authentication.createToken(u);
        if(userToken == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return ok().entity(userToken).build();
    }

    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response logOut(@HeaderParam("token")String token, String jsonBody) throws InterruptedException {
        System.out.println(jsonBody);
        System.out.println(token);
        User u = User.createUserFromJson(jsonBody);

        if (authentication.removeToken(token, u)){
            return Response.ok().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
