package at.htl.musicplayer.rest;

import at.htl.musicplayer.authentication.Authentication;
import at.htl.musicplayer.entity.Song;
import at.htl.musicplayer.facade.SongFacade;
import at.htl.musicplayer.loader.SongLoader;
import at.htl.musicplayer.utils.JsonConverter;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Base64;

@Path("song")
public class MusicAccess {
    @Inject
    SongFacade songFacade;

    @Inject
    Authentication authentication;

    @GET
    public Response getStartPage(){
        return Response.ok().entity("Webserver is working").build();
    }

    @GET
    @Path("/id={id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSongById(@PathParam("id") long id, @HeaderParam("token") String token) throws IOException, URISyntaxException {
        if (!authentication.isTokenValid(token)){
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        Song song = songFacade.get(id);
        JSONObject jsonSong = song.getJsonObject();
        jsonSong.put("base64File", SongLoader.getMusicFileAsBase64(song.getPath()));
        return Response.ok().entity(jsonSong.toString()).build();
    }

    @POST
    @Path("/addSongs")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addSongs(@HeaderParam("token") String token, String body){
        if(authentication.isTokenFromAdmin(token)){
            songFacade.saveAll(JsonConverter.jsonToSongListDecoded(body));
            return Response.ok().build();
        }
        else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @GET
    @Path("/like={search}")
    public Response getSongsLike(@HeaderParam("token")String token, @PathParam("search")String searchCoded){
        boolean isvalid= authentication.isTokenValid(token);
        if(isvalid){
            JSONObject jsonSongs = new JSONObject();
            String search = new String(Base64.getDecoder().decode(searchCoded));
            jsonSongs.put("songs", JsonConverter.songListToJsonArray(songFacade.getSongsContaining(search)));
            return Response.ok().entity(jsonSongs.toString()).entity(jsonSongs.toString()).build();
        }
        else{
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }
}
