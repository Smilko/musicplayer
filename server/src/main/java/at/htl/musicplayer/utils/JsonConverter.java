package at.htl.musicplayer.utils;

import at.htl.musicplayer.entity.Song;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

public class JsonConverter {
    public static List<Song> jsonToSongListDecoded(String json){
        List<Song> songs = new LinkedList<>();
        JSONObject jsonObject = new JSONObject(json);
        JSONArray array = jsonObject.getJSONArray("files");
        for (Object file: array) {
            songs.add(jsonObjToSongDecoded((JSONObject) file));
        }
        return songs;
    }

    public static Song jsonObjToSongDecoded(JSONObject jsonObject){
//        return new Song(null, new String(Base64.getDecoder().decode(jsonObject.getString("title").getBytes())),
//                new String(Base64.getDecoder().decode(jsonObject.getString("filePath").getBytes())));
        return new Song(null, jsonObject.getString("title"), jsonObject.getString("filePath"));
    }

    public static JSONArray songListToJsonArray(List<Song> songs){
        JSONArray array = new JSONArray();
        for (Song s : songs) {
            array.put(s.getJsonObject(false));
        }
        return array;
    }
}
