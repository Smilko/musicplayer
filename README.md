# MusicPlayer Readme

## 1 Systemarchitekrur
![deploymentDiagram](images/Deploymentdiagram.png)

## 2 UseCase
![usecaseDiagram](images/useCase.png)

## 3 Aufbau
Die Applikation MusicPlayer wurde in 3 Teile unterteit. Zum einem dem JavaEE Server, dem JavaFX Client und dem Java Songfinder.

### 3.1 Server
Der Server besitzt als einziger eine Datenbankverbindung. In der Datenbank werden Daten über nutzer und Songs gespeichert, aber nicht die Bytedaten der Songs. Anstatt der Specherung der Bytedaten wird auf die Datei verwiesen.

### 3.2 Songfinder
Da in der Datenbank keine Kompletten MP3 Datein gespeichert müssen die Datein auf der Festplatte des Servers liegen. Die UploaderApplikation  mauss auf dem selben Rechner wie die ServerApplikation liegen da die Datein sonst nicht erreichbar wären.

#### Wichtige Konfigurationen
In der config.properties Datei innerhalb des SongfiderProjektes liegt eine Variable `startPath`.  Diese Variable muss an das eigene System angepasst werden, da nur in diesem und in den Darunterliegenden Ordnern nach Musikdatein gesucht wird.

### 3.3 Client
JavaFX bietet hohe Felxibilität und Kontrolle über den Ablauf des Programmes und wurde aufgrund dessen als Frontendlösung hergenommen.


## 4 User
Das Hochladen von Musikdatein sollten nur über ausgewählte Nutzer erfolgen. Deswegen wird beim ersten Start der ServerApplikation ein Admin Nutzer angelegt.

Admin Nutzer:
- Username: admin
- Password: admin

## 5 Startablauf

1. Starten der Datenbank
2. Starten der Server-Applikation
3. Konfiguration des Songfinder beachten
4. Starten des Songfinders, falls benötigt
5. Client starten und Applikation
