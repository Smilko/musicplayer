package at.htl.utils;

import at.htl.entity.Song;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class JsonConverter {
    public static List<Song> jsonToSongList(JSONObject jsonObject, String arrayName){
        List<Song> songs = new LinkedList<>();
        JSONArray array = jsonObject.getJSONArray(arrayName);
        for (Object song: array) {
            songs.add(Song.getSongFromJson((JSONObject) song));
        }
        return songs;
    }

    public static Song jsonObjToSong(JSONObject jsonObject){
        return new Song(null, jsonObject.getString("title"), jsonObject.getString("filePath"));
    }
}
