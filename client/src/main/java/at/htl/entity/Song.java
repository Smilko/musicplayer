package at.htl.entity;

import org.json.JSONObject;

public class Song {

    private long id;

    private Artist artist;

    private String name;

    String path;

    //region Constr
    public Song(Artist artist, String name, String path) {
        this.artist = artist;
        this.name = name;
        this.path = path;
    }

    public Song() {
    }
    //endregion

    //region Getter and Setter

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artistid) {
        this.artist = artistid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    //endregion

    public JSONObject getJsonObject(){
        return getJsonObject(false);
    }

    public JSONObject getJsonObject(boolean putpath){
        JSONObject json = new JSONObject();
        json.put("id", getId());
        json.put("artistId", getArtist().getId());
        json.put("name", getName());
        if (putpath)
            json.put("path", getPath());
        return json;
    }

    public static Song getSongFromJson(JSONObject jsonObject){
        Song s = new Song();
        s.setId((jsonObject.getLong("id")));
        s.setName((String) jsonObject.get("name"));
        return s;
    }
}