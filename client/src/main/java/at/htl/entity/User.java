package at.htl.entity;

import org.json.JSONObject;

public class User {
    private long id;
    private String username;
    private String password;
    private String firstname;
    private String lastname;

    //region Constr
    public User(String username, String password, String firstname, String lastname) {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public User() {
    }
    //endregion

    //region Getter and Setter
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    //endregion


    @Override
    public boolean equals(Object obj) {
        User other = (User) obj;
        return this.getUsername().equals(other.getUsername());
    }

    public boolean isValid(User oth){
        return this.getUsername().equals(oth.getUsername()) && this.getPassword().equals(oth.getPassword());
    }

    public static User createUserFromJson(String json){
        JSONObject person = new JSONObject(json);
        return new User(
                person.getString("username"),
                person.getString("password"),
                person.getString("firstname"),
                person.getString("lastname"));
    }

    public JSONObject parseJson(){
        return parseJson(true);
    }

    public JSONObject parseJson(boolean parsePassword){
        JSONObject json = new JSONObject();

        json.put("id", this.getId());
        json.put("username", this.getUsername());
        if(parsePassword)
            json.put("password", this.getPassword());
        json.put("firstname", this.getFirstname());
        json.put("lastname", this.getLastname());

        return json;
    }
}
