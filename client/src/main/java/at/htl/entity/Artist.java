package at.htl.entity;

public class Artist {

    private long id;

    private String name;

    //region Constructor
    public Artist(String name) {
        this.name = name;
    }

    public Artist() {
    }
    //endregion

    //region Getter and Setter
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    //endregion

}
