package at.htl.rest;

import at.htl.entity.Song;
import at.htl.entity.User;
import at.htl.utils.JsonConverter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import static org.apache.http.entity.mime.MIME.UTF8_CHARSET;

public class RestClient
{
    private String token = null;
    private BasicRestCalls basicCalls;
    private static RestClient instance = null;

    private RestClient(){
        basicCalls = BasicRestCalls.getInstance();
    }

    public static RestClient getInstance(){
        if(instance == null){
            instance = new RestClient();
        }
        return instance;
    }

    public int login(User u){
        try {
            String s = u.parseJson().toString();
            HttpResponse r = basicCalls.post("access/login", new StringEntity(s), token);
            token = extractString(r.getEntity());
            System.out.println("Got token: " + token);
            return r.getStatusLine().getStatusCode();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return HttpStatus.SC_SERVICE_UNAVAILABLE;
    }

    public HttpResponse register(User user) throws IOException, NoSuchFieldException {
        System.out.println("USER: " + user.parseJson().toString());
        return basicCalls.post("register", new StringEntity(user.parseJson().toString()), token);
    }

    public List<Song> getSongs(String search) throws NoSuchFieldException, IOException {
        String codedSearch = new String(Base64.getEncoder().encode(search.getBytes()));
        HttpResponse response = basicCalls.get("song/like=" + codedSearch, token);
        JSONObject r = new JSONObject(extractString(response.getEntity()));
        return JsonConverter.jsonToSongList(r, "songs");
    }

    public byte[] getMusicBytesDecoded(long id) throws NoSuchFieldException, IOException {
        HttpResponse response = basicCalls.get("song/id=" + id, token);
        String res = extractString(response.getEntity());
        JSONObject r = new JSONObject(res);
        return Base64.getDecoder().decode(((String)r.get("base64File")).getBytes());
    }

    private String extractString(HttpEntity entity) throws IOException {
        return EntityUtils.toString(entity, UTF8_CHARSET).toString();
    }

}
