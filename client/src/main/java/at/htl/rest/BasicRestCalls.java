package at.htl.rest;

import at.htl.config.Properties;
import at.htl.config.Property;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class BasicRestCalls {
    private static BasicRestCalls instance = null;

    private BasicRestCalls(){
    }

    public static BasicRestCalls getInstance(){
        if(instance == null)
            instance = new BasicRestCalls();
        return instance;
    }

    public HttpResponse get(String path, String token) throws NoSuchFieldException, IOException {
    HttpClient httpclient = HttpClientBuilder.create().build();
    String url = String.format("%s/%s", Properties.getProperty(Property.serverURL), path);
    HttpGet getRequest = new HttpGet(url);
    System.out.println(url);

    getRequest.addHeader("token", token);
    getRequest.addHeader("content-type", "application/json");
    HttpResponse response = httpclient.execute(getRequest);
    System.out.println("Get request executed with response: " + response.getStatusLine());

    return response;
}

    public HttpResponse post(String path, StringEntity entity, String token) throws NoSuchFieldException, IOException {
        HttpClient httpclient = HttpClientBuilder.create().build();
        String url = String.format("%s/%s", Properties.getProperty(Property.serverURL), path);
        HttpPost postRequest = new HttpPost(url);

        postRequest.setEntity(entity);
        postRequest.addHeader("token", token);
        postRequest.addHeader("content-type", "application/json");
        HttpResponse response = httpclient.execute(postRequest);
        System.out.println("Post request executed with response: " + response.getStatusLine());

        return response;
    }
}
