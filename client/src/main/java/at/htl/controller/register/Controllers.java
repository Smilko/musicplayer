package at.htl.controller.register;

import at.htl.controller.base.CallableController;

import java.util.HashMap;

public class Controllers {
    private HashMap<String, CallableController> controllers;
    private static Controllers instance = null;

    private Controllers(){
        controllers = new HashMap<>();
    }

    public static Controllers getInstance() {
        if(instance == null)
            instance = new Controllers();
        return instance;
    }


    public void add(CallableController controller){
        controllers.put(controller.getClass().getName(), controller);
    }

    public CallableController get(String name){
        return controllers.get(name);
    }

}
