package at.htl.controller.register;

public class View {
    public static final String main = "view/main.fxml",
                    register = "view/register.fxml",
                    menu = "view/menu.fxml",
                    startPage = "view/startpage.fxml",
                    root="view/root.fxml",
                    setting="view/setting.fxml",
                    aboutMe="view/aboutMe.fxml",
                    searchBox="view/search.fxml",
                    searchBody="view/searchBody.fxml",
                    bottomPlayer="view/bottomPlayer.fxml";
}
