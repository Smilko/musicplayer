package at.htl.controller.register;

import java.util.HashMap;

public class PaneControllerFactory {
    private HashMap<String, PaneController> controllers;

    private static PaneControllerFactory instance = null;

    private PaneControllerFactory(){
        controllers = new HashMap<>();
    }

     public static PaneControllerFactory getInstance(){
        if(instance == null)
            instance = new PaneControllerFactory();
        return instance;
     }

     public PaneController getSceneController(String scenename){
        if(!controllers.containsKey(scenename))
            putSceneController(scenename, new PaneController());
        return controllers.get(scenename);
     }

     public void putSceneController(String name, PaneController controller){
        if(!controllers.containsKey(name)){
            controllers.put(name, controller);
            return;
        }
         System.out.println("Warning: Name \'" + name + "\' already exists");
     }
}
