package at.htl.controller.register;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

import java.util.HashMap;

public class PaneController {
    private HashMap<String, Pane> hashMap = null;
    private BorderPane main = null;

    public PaneController() {
        hashMap = new HashMap<>();
    }

    public void setMainPane(BorderPane mainScene){
        this.main = mainScene;
    }

    public void add(String name, Pane pane){
        hashMap.put(name, pane);
    }

    public void remove(String name){
        hashMap.remove(name);
    }

    public void activate(String name){
        main.setCenter(hashMap.get(name));
    }
}
