package at.htl.controller;

import at.htl.controller.register.PaneController;
import at.htl.controller.register.PaneControllerFactory;
import at.htl.controller.register.Scenes;
import at.htl.controller.register.View;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class MenueController {

    @FXML
    private BorderPane mainPane;

    public void initialize() throws IOException {
        PaneController show = PaneControllerFactory.getInstance().getSceneController(Scenes.playerScene);
        show.setMainPane(mainPane);
        show.add(View.startPage, FXMLLoader.load(getClass().getClassLoader().getResource(View.startPage)));
        show.add(View.setting, FXMLLoader.load(getClass().getClassLoader().getResource(View.setting)));
        show.add(View.aboutMe, FXMLLoader.load(getClass().getClassLoader().getResource(View.aboutMe)));
        show.add(View.searchBody, FXMLLoader.load(getClass().getClassLoader().getResource(View.searchBody)));
        show.activate(View.startPage);

        mainPane.setLeft(FXMLLoader.load(getClass().getClassLoader().getResource(View.searchBox)));
        mainPane.getLeft().prefHeight(150);
        mainPane.setBottom(FXMLLoader.load(getClass().getClassLoader().getResource(View.bottomPlayer)));
    }

    public void onHome(ActionEvent actionEvent) {
        PaneControllerFactory.getInstance().getSceneController(Scenes.playerScene).activate(View.startPage);
    }

    public void onSetting(ActionEvent actionEvent) {
        PaneControllerFactory.getInstance().getSceneController(Scenes.playerScene).activate(View.setting);
    }

    public void onAboutMe(ActionEvent actionEvent) {
        PaneControllerFactory.getInstance().getSceneController(Scenes.playerScene).activate(View.aboutMe);
    }

    public void logOut(ActionEvent actionEvent) {
        PaneControllerFactory.getInstance().getSceneController(Scenes.startScene).activate(View.main);
    }

    public void onAboutThisProject(ActionEvent actionEvent) {

    }
}
