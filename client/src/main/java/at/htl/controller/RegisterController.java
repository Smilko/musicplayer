package at.htl.controller;

import at.htl.controller.register.PaneControllerFactory;
import at.htl.controller.register.Scenes;
import at.htl.controller.register.View;
import at.htl.entity.User;
import at.htl.rest.RestClient;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

import java.io.IOException;

public class RegisterController{

    @FXML
    private TextField password;
    @FXML
    private TextField username;
    @FXML
    private TextField fistName;
    @FXML
    private TextField lastName;


    private User user;

    public void onBack(){
        PaneControllerFactory.getInstance().getSceneController(Scenes.startScene).activate(View.main);
    }

    public void onRegister() throws IOException, NoSuchFieldException {
        HttpResponse response =  RestClient.getInstance().register(
                new User(username.getText(), password.getText(), fistName.getText(), lastName.getText()));
        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_CONFLICT){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("User already exists!");
            a.show();
        }else if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Could not send data to server!");
            a.show();
        }
        else {
            clean();
            PaneControllerFactory.getInstance().getSceneController(Scenes.startScene).activate(View.main);
        }
    }

    private void clean(){
        password.clear();
        username.clear();
        fistName.clear();
        lastName.clear();
    }
}
