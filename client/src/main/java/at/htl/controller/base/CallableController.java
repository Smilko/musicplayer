package at.htl.controller.base;

import at.htl.controller.register.Controllers;

public abstract class CallableController {
    public CallableController(){
        Controllers.getInstance().add(this);
    }
}
