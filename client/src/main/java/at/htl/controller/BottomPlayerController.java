package at.htl.controller;

import at.htl.config.Properties;
import at.htl.config.Property;
import at.htl.controller.base.CallableController;
import at.htl.rest.RestClient;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class BottomPlayerController extends CallableController {
    private boolean isPlaying = false;
    private MediaPlayer player = null;
    private final String playPath = "images/if_music_play_pause_control_go_arrow_1868967.png";
    private final String pausePath = "images/if_music_pause_stop_control_play_1868969.png";
    private Thread progress = null;
    private boolean kill = false;

    @FXML
    private Label title;

    @FXML
    private Button playPause;

    @FXML
    private ProgressBar progressBar;

    @FXML
    public void onPlayButton(ActionEvent actionEvent) {
        if(isPlaying)
            pause();
        else
            play();
    }

    public void initialize() {
        playPause.setGraphic(createImageView(playPath));
    }

    private void play(){
        player.play();
        isPlaying = true;
        playPause.setGraphic(createImageView(pausePath));
        kill = false;
        progress = new Thread(() -> {
            while (!kill){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> {
                    progressBar.setProgress(player.getCurrentTime().toMillis() / player.getTotalDuration().toMillis());
                });
            }
        });
        progress.start();
    }

    private void pause(){
        player.pause();
        isPlaying = false;
        playPause.setGraphic(createImageView(playPath));
        kill = true;
    }

    public void shutdown(){
        kill = true;
    }

    private ImageView createImageView(String file){
        Image image = new Image(getClass().getClassLoader().getResourceAsStream(file));
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(15);
        imageView.setFitWidth(15);
        return imageView;
    }


    public void playSong(long id, String title){
        this.title.setText(title);
        if(player != null){
            pause();
        }
        String file = "";

        try {
            downloadAndCreateFile(id);
            file = Properties.getProperty(Property.tempFile);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(player != null){
            pause();
        }
        Media pick = new Media(new File(file).toURI().toASCIIString());
        player = new MediaPlayer(pick);
        play();
        System.out.println("Playing song with id: " + id);
    }

    private void downloadAndCreateFile(long id) throws NoSuchFieldException, IOException {
        String file = Properties.getProperty(Property.tempFile);
        File f = new File((new File("." + '/' + file).getAbsolutePath()));
        if(!f.exists()){
            f.createNewFile();
        }

        OutputStream o = new FileOutputStream(f);
        o.write(RestClient.getInstance().getMusicBytesDecoded(id));
        o.close();
    }

    public void onProgressCkicked(MouseEvent mouseEvent) {
        double jumpToMillis = (mouseEvent.getX()/progressBar.getWidth())* player.getTotalDuration().toMillis();
        player.seek(new Duration(jumpToMillis));
    }
}
