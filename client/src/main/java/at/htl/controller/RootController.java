package at.htl.controller;

import at.htl.controller.register.PaneController;
import at.htl.controller.register.PaneControllerFactory;
import at.htl.controller.register.Scenes;
import at.htl.controller.register.View;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class RootController{
    @FXML
    private BorderPane mainPane;

    public void initialize() throws IOException {
        PaneController startScene = PaneControllerFactory.getInstance().getSceneController(Scenes.startScene);
        startScene.add(View.main, FXMLLoader.load(getClass().getClassLoader().getResource(View.main)));
        startScene.add(View.register, FXMLLoader.load(getClass().getClassLoader().getResource(View.register)));
        startScene.add(View.menu, FXMLLoader.load(getClass().getClassLoader().getResource(View.menu)));

        startScene.setMainPane(mainPane);
        startScene.activate(View.main);
    }
}
