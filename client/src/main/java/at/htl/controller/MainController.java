package at.htl.controller;

import at.htl.controller.register.PaneControllerFactory;
import at.htl.controller.register.Scenes;
import at.htl.controller.register.View;
import at.htl.entity.User;
import at.htl.rest.RestClient;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import org.apache.http.HttpStatus;

import java.io.IOException;

public class MainController {

    @FXML
    private Label errortext;
    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    public void openRegister() throws IOException {
        System.out.println("Register");
        PaneControllerFactory.getInstance().getSceneController(Scenes.startScene).activate(View.register);
    }

    public void login(){
        System.out.println("Login");
        int code = RestClient.getInstance().login(new User(username.getText(), password.getText(), "", ""));
        if(code == HttpStatus.SC_OK)
        {
            System.out.println("Success");
            PaneControllerFactory.getInstance().getSceneController(Scenes.startScene).activate(View.menu);
            errortext.setText("");
            errortext.setTextFill(Paint.valueOf("#f00"));
        }
        else if(code == HttpStatus.SC_SERVICE_UNAVAILABLE){
            System.err.println("Uable to connecto to server");
            errortext.setText("Uable to connecto to server");
            errortext.setTextFill(Paint.valueOf("#f00"));

        }else{
            System.err.println("Wrong login");
            errortext.setText("Wrong login");
            errortext.setTextFill(Paint.valueOf("#f00"));

        }
    }
}
