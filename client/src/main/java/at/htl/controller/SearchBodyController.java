package at.htl.controller;

import at.htl.controller.base.CallableController;
import at.htl.controller.register.Controllers;
import at.htl.entity.Song;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.LinkedList;
import java.util.List;

public class SearchBodyController extends CallableController {

    @FXML
    private VBox box;

    public SearchBodyController() {
        super();
    }


    public void loadList(List<Song> songs){
        System.out.println("Creating list of length: " + songs.size());
        box.getChildren().clear();

        Thread t = new Thread(() -> {
            List<Node> nodes = new LinkedList();
            for (Song song : songs) {
                nodes.add(createEntries(song));
            }
            Platform.runLater(() -> {
                if(nodes.size() != 0)
                    box.getChildren().setAll(nodes);
                else {
                    Label noContentText = new Label("Nothing found!");
                    box.getChildren().add(noContentText);
                }
            });
        });
        t.start();
    }


    public HBox createEntries(Song s){
        HBox box = new HBox();
        Image playImage = new Image(getClass().getClassLoader().
                getResourceAsStream("images/if_music_play_pause_control_go_arrow_1868967.png"));
        ImageView imageView = new ImageView(playImage);
        imageView.setFitHeight(15);
        imageView.setFitWidth(15);
        Button play = new Button();
        play.setGraphic(imageView);
        //String showText = new String(Base64.getDecoder().decode(s.getName().getBytes()));
        Label text = new Label(s.getName());
        play.maxHeight(15.0);
        play.setOnAction(event -> {
            playSong(s.getId(), s.getName());
        });

        box.getChildren().add(play);
        box.getChildren().add(text);

        return box;
    }

    private void playSong(Long id, String title) {
        BottomPlayerController controller = (BottomPlayerController) Controllers.getInstance()
                .get(BottomPlayerController.class.getName());
        controller.playSong(id, title);
    }
}
