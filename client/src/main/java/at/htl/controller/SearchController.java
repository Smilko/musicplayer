package at.htl.controller;

import at.htl.controller.register.Controllers;
import at.htl.controller.register.PaneControllerFactory;
import at.htl.controller.register.Scenes;
import at.htl.controller.register.View;
import at.htl.entity.Song;
import at.htl.rest.RestClient;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.util.List;

public class SearchController {

    @FXML
    private TextField searchField;

    @FXML
    private void startSearching() throws NoSuchFieldException, IOException {
        List<Song> songs = RestClient.getInstance().getSongs(searchField.getText());
        SearchBodyController contr =  ((SearchBodyController)Controllers.getInstance()
                .get(SearchBodyController.class.getName()));
        PaneControllerFactory.getInstance().getSceneController(Scenes.playerScene).activate(View.searchBody);
        contr.loadList(songs);
    }
}
