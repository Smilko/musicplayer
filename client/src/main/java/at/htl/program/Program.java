package at.htl.program;

import at.htl.controller.register.View;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Program extends Application{

    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(View.root));
        Scene s = new Scene(root);
        primaryStage.setScene(s);
        primaryStage.show();
    }
}
